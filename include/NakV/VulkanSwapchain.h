#pragma once

#include <vulkan/vulkan.h>

#include <vector>

namespace nakv
{
  class VulkanContext;

  class VulkanDevice;

  class VulkanWindow;

  struct SwapchainProps {
    
    VkSurfaceFormatKHR _surfaceFormat;
    
    VkPresentModeKHR _presentMode;
    
    VkExtent2D _extent;
    
  };

  struct SwapchainResources {

    VkRenderPass _renderPass = VK_NULL_HANDLE;

    std::vector<VkImage> _images;

    std::vector<VkImageView> _imageViews;

    std::vector<VkFramebuffer> _framebuffers;

  };

  struct DepthResources {

    VkFormat _format;

    VkImage _image = VK_NULL_HANDLE;

    VkDeviceMemory _imageMemory = VK_NULL_HANDLE;

    VkImageView _imageView = VK_NULL_HANDLE;

  };

  class VulkanSwapchain
  {
    public:

      VulkanSwapchain ();

      VulkanSwapchain (const VulkanContext* context);

      ~VulkanSwapchain ();

      VkSwapchainKHR _swapchain;

      SwapchainProps _properties;

      SwapchainResources _resources;

      DepthResources _depth;

      void init (const VulkanContext* context);

      void init ();

      void cleanup ();

      operator VkSwapchainKHR ()
      {
        return _swapchain;
      }

    private:

      const VulkanContext* _context;

      const VulkanDevice* _device;

      const VulkanWindow* _window;

      VkSurfaceFormatKHR chooseSurfaceFormat ();

      VkPresentModeKHR choosePresentMode ();

      VkExtent2D chooseSurfaceExtent ();

      void initResourceImagesAndViews ();

      void initFramebuffers ();
  };
}