#pragma once

#include <vulkan/vulkan.h>

namespace nakv
{

  void NAKV_CHECK (bool check, const char* errorMsg);

  void NAKV_VALIDATE (VkResult result, const char* errorMsg);

}