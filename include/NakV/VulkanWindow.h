#pragma once

#ifndef GLFW_INCLUDE_VULKAN
  #define GLFW_INCLUDE_VULKAN
  #include <GLFW/glfw3.h>
#endif

#include <iostream>

#include <NakV/VulkanContext.h>

namespace nakv{

  class VulkanWindow {

    public: 
      VulkanWindow();

      VulkanWindow(int width, int height, const char* title);

      ~VulkanWindow();

      /** Window width */
      unsigned int _width;
      /** Window height */
      unsigned int _height;
      /** Window title */
      std::string _title;
      /** True when _glfw has resized */
      bool _hasFramebufferResized = false;

      /** Pointer to the GLFWwindow instance */
      GLFWwindow* _glfw;
      /** VulkanSurfaceKHR handle */
      VkSurfaceKHR _surface = VK_NULL_HANDLE;

      /**  */
      static void framebufferResizeCallback (GLFWwindow* glfwWindow, int width, int height);

      static void pollEvents ();

      /** Inits glfw window */
      void init ();

      /** Checks if window should close (should happen every frame) */
      bool shouldClose ();

      /** Polls events (should happen every frame) */
      // void update  ();

  };

}