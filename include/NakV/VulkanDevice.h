#pragma once

#include <vulkan/vulkan.h>

#include <memory>

namespace nakv {

  struct GPUInfo;

  struct GPUQueueFamilyIndices;
  
  class VulkanContext;

  struct DeviceCommandPools {
    
    VkCommandPool _graphics = VK_NULL_HANDLE;
    
    VkCommandPool _transfer = VK_NULL_HANDLE;

  };

  struct DeviceQueues {

    VkQueue _graphics = VK_NULL_HANDLE;

    VkQueue _present = VK_NULL_HANDLE;

    VkQueue _transfer = VK_NULL_HANDLE;

  };

  class VulkanDevice {
    
    public:

      VulkanDevice ();

      VulkanDevice (const VulkanContext* context, GPUInfo* gpu);

      ~VulkanDevice ();

      VkDevice _logicalDevice = VK_NULL_HANDLE;

      GPUInfo* _gpu;

      GPUQueueFamilyIndices* _queueIndices;

      DeviceQueues _queues;

      DeviceCommandPools _commandPools;

      static bool hasStencilComponent (VkFormat format);

      void init (const VulkanContext* context, GPUInfo* gpu);

      void init ();

      void allocateCommandBuffers (VkCommandPool commandPool, const uint32_t bufferCount, VkCommandBuffer* pCommandBuffer);

      void createImage (
        uint32_t width, uint32_t height, uint32_t mipLevels,
        VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, 
        uint32_t queueFamilyIndexCount, const uint32_t* pQueueFamilyIndices,
        VkMemoryPropertyFlags properties,
        VkImage& image, VkDeviceMemory& imageMemory
      ) const ;

      VkImageView createImageView (VkImage image, VkFormat format, VkImageAspectFlags aspectFlags, uint32_t mipLevels) const;

      void createImageView (VkImage image, VkImageView& imageView, VkFormat format, VkImageAspectFlags aspectFlags, uint32_t mipLevels) const;

      VkFormat chooseSupportedFormat (VkFormat* formats, uint32_t formatCount, VkImageTiling tiling, VkFormatFeatureFlags features) const;

      void transitionImageLayout (
        VkImage image, VkFormat format,
        VkImageLayout oldLayout, VkImageLayout newLayout,
        uint32_t mipLevels
      ) const;

      void cleanup ();

      operator VkDevice () {
        return _logicalDevice;
      }

    private:

      const VulkanContext* _context;

      void initCommandPools ();

      uint32_t findMemoryType (uint32_t typeFilter, VkMemoryPropertyFlags properties) const;

      VkCommandBuffer beginSingleTimeCommands(VkCommandPool commandPool) const;

      void endSingleTimeCommands(VkCommandPool commandPool, VkQueue queue, VkCommandBuffer commandBuffer) const;

  };

}


