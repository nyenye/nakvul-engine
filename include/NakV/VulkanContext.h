#pragma once

#include <vulkan/vulkan.h>

#include <iostream>
#include <memory>
#include <vector>

#include <NakV/VulkanDevice.h>
#include <NakV/VulkanSwapchain.h>

namespace nakv {

  class VulkanWindow;

  struct GPUQueueFamilyIndices {

    uint32_t _graphics = UINT32_MAX;
    
    uint32_t _present = UINT32_MAX;

    uint32_t _transfer = UINT32_MAX;

    // uint32_t _compute = UINT32_MAX;

    bool hasGraphicsQueue ();

    bool hasPresentQueue ();

    bool hasTransferQueue ();

    bool hasDedicatedPresentQueue ();

    bool hasDedicatedTransferQueue ();
    
  };

  struct GPUInfo {

    VkPhysicalDevice _physicalDevice;
    
    VkPhysicalDeviceProperties _properties;
    
    VkPhysicalDeviceFeatures _features;

    VkPhysicalDeviceMemoryProperties _memoryProperties;
    
    std::vector<VkQueueFamilyProperties> _queueFamiliesProps;
    
    std::vector<VkExtensionProperties> _extensions;
    
    VkSurfaceCapabilitiesKHR _capabilities;

    std::vector<VkSurfaceFormatKHR> _surfaceFormats;

    std::vector<VkPresentModeKHR> _presentModes;

    GPUQueueFamilyIndices _queueFamilies;

  };

  class VulkanContext {

    public:

      VulkanContext();

      // VulkanContext(std::shared_ptr<VulkanWindow> window);

      VulkanContext(VulkanWindow* window);

      ~VulkanContext();

      static const uint32_t FRAMES_IN_FLIGHT;
      static const std::vector<const char*> DEVICE_EXTENSIONS;
      static const std::vector<const char*> VALIDATION_LAYERS; 

      std::string _applicationName;
      uint32_t _applicationVersion;

      bool _validationLayersEnabled;
      std::vector<const char*> _validationLayers;      

      VulkanWindow* _window;

      VulkanDevice _device;

      VulkanSwapchain _swapchain;

      std::vector<VkCommandBuffer> _commandBuffers; 

      std::vector<VkSemaphore> _acquireSemaphores;
      std::vector<VkSemaphore> _renderCompleteSemaphores;
      std::vector<VkFence> _commandBufferFences;

      VkRenderPass _renderPass;

      static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback (
        VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
        VkDebugUtilsMessageTypeFlagsEXT messageType,
        const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
        void* pUserData
      );

      void init ();

      void cleanup ();

    private:

      VkInstance _instance = VK_NULL_HANDLE;

      VkDebugUtilsMessengerEXT _debugMessenger = VK_NULL_HANDLE;

      std::vector<GPUInfo> _gpus;

      uint32_t _selectedGPUIndex = UINT32_MAX;

      void createInstance ();

      void setupDebugMessenger ();

      void createSurface ();

      uint32_t findQueueFamilyIndex(const VkQueueFamilyProperties* queues, const uint32_t queueCount, VkQueueFlagBits flags);

      void queryPhysicalDevices ();

      void selectPhysicalDevice ();

      void createCommandBuffers ();

      void createSemaphores ();

      void createFences ();

      void createRenderPass ();

      void destroyDebugUtilsMessengerEXT (VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator);

      bool checkValidationLayerSupport (const std::vector<const char*> validationLayers);

      std::vector<const char*> getRequiredExtensions (const bool validationLayersEnabled);

      void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo);

      VkResult createDebugUtilsMessengerEXT(
        const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkDebugUtilsMessengerEXT* pDebugMessenger
      );

      bool isGPUSuitable (const GPUInfo& gpuInfo);

      bool checkDeviceExtensionSupport (const VkExtensionProperties* availableExtensions, const uint32_t extensionCount);
      
  };

}