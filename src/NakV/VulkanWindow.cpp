#include <NakV/VulkanWindow.h>

const uint32_t DEFAULT_WINDOW_WIDTH = 1080;
const uint32_t DEFAULT_WINDOW_HEIGHT = 920;
const char* DEFAULT_WINDOW_TITLE = "NakV";

nakv::VulkanWindow::VulkanWindow () {
  _width = DEFAULT_WINDOW_WIDTH;
  _height = DEFAULT_WINDOW_HEIGHT;
  _title = DEFAULT_WINDOW_TITLE;
}

nakv::VulkanWindow::VulkanWindow (int width, int height, const char* title) {
  _width = width;
  _height = height;
  _title = title != nullptr ? title : DEFAULT_WINDOW_TITLE;
}

nakv::VulkanWindow::~VulkanWindow () {
  glfwDestroyWindow(_glfw);
}

void nakv::VulkanWindow::framebufferResizeCallback (GLFWwindow* glfwWindow, int width, int height) {
  VulkanWindow* window = reinterpret_cast<VulkanWindow*>(glfwGetWindowUserPointer(glfwWindow));

  window->_hasFramebufferResized = true;
  window->_width = width;
  window->_height = height;
}

void nakv::VulkanWindow::pollEvents ()  {
  glfwPollEvents();
}

void nakv::VulkanWindow::init () {
  glfwInit();

  glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

  _glfw = glfwCreateWindow(
    _width,
    _height,
    _title.c_str(),
    nullptr,nullptr
  );

  glfwSetWindowUserPointer(_glfw, this);
  glfwSetFramebufferSizeCallback(_glfw, nakv::VulkanWindow::framebufferResizeCallback);
}

bool nakv::VulkanWindow::shouldClose () {
  return glfwWindowShouldClose(_glfw);
}

// void nakv::VulkanWindow::update () {
// }