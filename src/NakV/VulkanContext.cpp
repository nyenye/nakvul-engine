#include <NakV/VulkanContext.h>

#include <NakV/VulkanValidator.h>
#include <NakV/VulkanWindow.h>

#include <cstring>
#include <iostream>
#include <set>

std::string ENGINE_NAME = "NakV";
const uint32_t ENGINE_VERSION = VK_MAKE_VERSION(1, 0, 0);

bool nakv::GPUQueueFamilyIndices::hasGraphicsQueue ()
{
  return _graphics != UINT32_MAX;
}

bool nakv::GPUQueueFamilyIndices::hasPresentQueue ()
{
  return _present != UINT32_MAX;
}

bool nakv::GPUQueueFamilyIndices::hasTransferQueue ()
{
  return _transfer != UINT32_MAX;
}

bool nakv::GPUQueueFamilyIndices::hasDedicatedPresentQueue ()
{
  return hasPresentQueue() && _present != _graphics;
}

bool nakv::GPUQueueFamilyIndices::hasDedicatedTransferQueue ()
{
  return hasTransferQueue() && _transfer != _graphics;
}

const uint32_t nakv::VulkanContext::FRAMES_IN_FLIGHT = 2;

const std::vector<const char*> nakv::VulkanContext::DEVICE_EXTENSIONS = {
  VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

const std::vector<const char*> nakv::VulkanContext::VALIDATION_LAYERS = {
  "VK_LAYER_LUNARG_standard_validation",
  "VK_LAYER_KHRONOS_validation"
};

nakv::VulkanContext::VulkanContext ()
{
}

// nakv::VulkanContext::VulkanContext (std::shared_ptr<VulkanWindow> window)
// {
//   _window = window;
//   _validationLayersEnabled = false;
// }

nakv::VulkanContext::VulkanContext (nakv::VulkanWindow* window)
{
  _window = window;
  _validationLayersEnabled = false;
}

nakv::VulkanContext::~VulkanContext ()
{
  // if (_validationLayersEnabled) {
  //   nakv::VulkanContext::destroyDebugUtilsMessengerEXT(_instance, _debugMessenger, nullptr);
  // }

  // if (_window->_surface != VK_NULL_HANDLE)
  //   vkDestroySurfaceKHR(_instance, _window->_surface, nullptr);
  
  // if (_instance != VK_NULL_HANDLE)
  //   vkDestroyInstance(_instance, nullptr);
}

VKAPI_ATTR VkBool32 VKAPI_CALL nakv::VulkanContext::debugCallback (
  VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
  VkDebugUtilsMessageTypeFlagsEXT messageType,
  const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
  void* pUserData
)
{
  std::cerr << "validation layer: " << pCallbackData->pMessage << std::endl;

  return VK_FALSE;
}

void nakv::VulkanContext::init () 
{
  this->createInstance();
  this->setupDebugMessenger();
  this->createSurface();
  this->queryPhysicalDevices();
  this->selectPhysicalDevice();

  // Creates logical device, retrieves queues and creates command pools for respective queues
  _device.init(this, &(_gpus[_selectedGPUIndex]));

  this->createCommandBuffers();
  this->createSemaphores();
  this->createFences();

  // Creates swapchain, swapchain images, image views, and framebuffer
  _swapchain.init(this);

  // this->createRenderPass();
}

void nakv::VulkanContext::cleanup ()
{
  vkDeviceWaitIdle(_device);

  _swapchain.cleanup();

  // Destroy Fences
  for (size_t i = 0; i < VulkanContext::FRAMES_IN_FLIGHT; i++)
  {
    vkDestroyFence(_device, _commandBufferFences[i], nullptr);
  }
  
  // Destroy Semaphores
  for (size_t i = 0; i < VulkanContext::FRAMES_IN_FLIGHT; i++)
  {
    vkDestroySemaphore(_device, _acquireSemaphores[i], nullptr);
    vkDestroySemaphore(_device, _renderCompleteSemaphores[i], nullptr);
  }

  // Destroy Command Buffers
  vkFreeCommandBuffers(_device, _device._commandPools._graphics, VulkanContext::FRAMES_IN_FLIGHT, _commandBuffers.data());  
  
  _device.cleanup();

  if (_validationLayersEnabled) {
    nakv::VulkanContext::destroyDebugUtilsMessengerEXT(_instance, _debugMessenger, nullptr);
  }

  if (_window->_surface != VK_NULL_HANDLE)
    vkDestroySurfaceKHR(_instance, _window->_surface, nullptr);
  
  if (_instance != VK_NULL_HANDLE)
    vkDestroyInstance(_instance, nullptr);
}

void nakv::VulkanContext::createInstance ()
{
  if (_window->_glfw == nullptr)
  {
    std::runtime_error("_window must be initialized");
  }

  if (
    _validationLayersEnabled &&
    !nakv::VulkanContext::checkValidationLayerSupport(_validationLayers)
  )
  {
    throw std::runtime_error("validation layers requested, but not available!");
  }

  VkApplicationInfo appInfo = {};
  appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
  appInfo.pApplicationName = _applicationName.c_str();
  appInfo.applicationVersion = _applicationVersion;
  appInfo.pEngineName = ENGINE_NAME.c_str();
  appInfo.engineVersion = ENGINE_VERSION;
  appInfo.apiVersion = VK_API_VERSION_1_0;

  std::vector<const char*> extensions = nakv::VulkanContext::getRequiredExtensions(_validationLayersEnabled);

  VkInstanceCreateInfo instanceInfo = {};
  instanceInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
  instanceInfo.pApplicationInfo = &appInfo;
  instanceInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
  instanceInfo.ppEnabledExtensionNames = extensions.data();

  uint32_t availableExtensionCount = 0;
  vkEnumerateInstanceExtensionProperties(nullptr, &availableExtensionCount, nullptr);
  std::vector<VkExtensionProperties> availableExtensions(availableExtensionCount);
  vkEnumerateInstanceExtensionProperties(nullptr, &availableExtensionCount, availableExtensions.data());
  
  VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo;
  if (_validationLayersEnabled)
  {
    instanceInfo.enabledLayerCount = static_cast<uint32_t>(_validationLayers.size());
    instanceInfo.ppEnabledLayerNames = _validationLayers.data();

    nakv::VulkanContext::populateDebugMessengerCreateInfo(debugCreateInfo);
    instanceInfo.pNext = (VkDebugUtilsMessengerCreateInfoEXT*) &debugCreateInfo;
  }
  else
  {
    instanceInfo.enabledLayerCount = 0;
    instanceInfo.pNext = nullptr;
  }
  
  NAKV_VALIDATE(vkCreateInstance(&instanceInfo, nullptr, &_instance), "failed to create instance!");
}

void nakv::VulkanContext::setupDebugMessenger ()
{

  if (!_validationLayersEnabled) return;

  VkDebugUtilsMessengerCreateInfoEXT createInfo;
  nakv::VulkanContext::populateDebugMessengerCreateInfo(createInfo);

  if (nakv::VulkanContext::createDebugUtilsMessengerEXT(&createInfo, nullptr, &_debugMessenger) != VK_SUCCESS) {
    throw std::runtime_error("failed to set up debug messenger!");
  }

}

void nakv::VulkanContext::createSurface ()
{
  NAKV_VALIDATE(
    glfwCreateWindowSurface(_instance, _window->_glfw, nullptr, &(_window->_surface)),
    "failed to create window surface"
  );
}

uint32_t nakv::VulkanContext::findQueueFamilyIndex(const VkQueueFamilyProperties* queues, const uint32_t queueCount, VkQueueFlagBits flags)
{
  if (flags & VK_QUEUE_COMPUTE_BIT)
  {
    for (uint32_t i = 0; i < queueCount; i++)
    {
      if (queues[i].queueCount > 0)
      {
        if ((queues[i].queueFlags & flags) && (queues[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) == 0)
        {
          return i;
        }
      }
    }
  }

  if (flags & VK_QUEUE_TRANSFER_BIT)
  {
    for (uint32_t i = 0; i < queueCount; i++)
    {
      if (queues[i].queueCount > 0)
      {
        if ((queues[i].queueFlags & flags) && (queues[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) == 0 && (queues[i].queueFlags & VK_QUEUE_COMPUTE_BIT) == 0)
        {
          return i;
        }
      }
    }
  }

  for (uint32_t i = 0; i < queueCount; i++)
  {
    if (queues[i].queueCount > 0)
    {
      if (queues[i].queueFlags & flags)
      {
        return i;
      }
    }
  }

  throw std::runtime_error("Could not find a suitable queue family index.");
}

void nakv::VulkanContext::queryPhysicalDevices ()
{
  uint32_t gpuCount = 0;
  std::vector<VkPhysicalDevice> devices;
  vkEnumeratePhysicalDevices(_instance, &gpuCount, nullptr);

  NAKV_CHECK(gpuCount > 0, "No available GPUs where found");
  
  devices.resize(gpuCount);
  vkEnumeratePhysicalDevices(_instance, &gpuCount, devices.data());

  NAKV_CHECK(gpuCount > 0, "No available GPUs where found");

  _gpus.resize(gpuCount);

  for (size_t i = 0; i < gpuCount; i++)
  {
    GPUInfo& gpu = _gpus[i];
    
    gpu._physicalDevice = devices[i];

    vkGetPhysicalDeviceProperties(gpu._physicalDevice, &gpu._properties);

    vkGetPhysicalDeviceFeatures(gpu._physicalDevice, &gpu._features);

    vkGetPhysicalDeviceMemoryProperties(gpu._physicalDevice, &gpu._memoryProperties);

    {
      uint32_t queueFamilyCount;
      vkGetPhysicalDeviceQueueFamilyProperties(gpu._physicalDevice, &queueFamilyCount, nullptr);

      if (queueFamilyCount > 0)
      {
        gpu._queueFamiliesProps.resize(queueFamilyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(gpu._physicalDevice, &queueFamilyCount, gpu._queueFamiliesProps.data());
      }
    }

    {
      uint32_t extensionCount;
      vkEnumerateDeviceExtensionProperties(gpu._physicalDevice, nullptr, &extensionCount, nullptr);

      if (extensionCount > 0)
      {
        gpu._extensions.resize(extensionCount);
        vkEnumerateDeviceExtensionProperties(gpu._physicalDevice, nullptr, &extensionCount, gpu._extensions.data());
      }
    }

    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(gpu._physicalDevice, _window->_surface, &gpu._capabilities);

    {
      uint32_t surfaceFormatCount;
      vkGetPhysicalDeviceSurfaceFormatsKHR(gpu._physicalDevice, _window->_surface, &surfaceFormatCount, nullptr);

      if (surfaceFormatCount > 0) {
        gpu._surfaceFormats.resize(surfaceFormatCount);
        vkGetPhysicalDeviceSurfaceFormatsKHR(gpu._physicalDevice, _window->_surface, &surfaceFormatCount, gpu._surfaceFormats.data());
      }
    }

    {
      uint32_t surfacePresentModeCount;
      vkGetPhysicalDeviceSurfacePresentModesKHR(gpu._physicalDevice, _window->_surface, &surfacePresentModeCount, nullptr);

      if (surfacePresentModeCount > 0) {
        gpu._presentModes.resize(surfacePresentModeCount);
        vkGetPhysicalDeviceSurfacePresentModesKHR(gpu._physicalDevice, _window->_surface, &surfacePresentModeCount, gpu._presentModes.data());
      }
    }

    // Find queue family indices
    gpu._queueFamilies._graphics = findQueueFamilyIndex(gpu._queueFamiliesProps.data(), gpu._queueFamiliesProps.size(), VK_QUEUE_GRAPHICS_BIT);

    // Find queue which supports present
    for (uint32_t presentIndex; presentIndex < gpu._queueFamiliesProps.size(); presentIndex++)
    {
      VkBool32 presentSupport = false;
      vkGetPhysicalDeviceSurfaceSupportKHR(gpu._physicalDevice, presentIndex, _window->_surface, &presentSupport);

      if (gpu._queueFamiliesProps[presentIndex].queueCount > 0 &&  presentSupport)
      {
        gpu._queueFamilies._present = presentIndex;
      }
    }
    
    // gpu._queueFamilies._compute = findQueueFamilyIndex(gpu._queueFamiliesProps.data(), gpu._queueFamiliesProps.size(), VK_QUEUE_COMPUTE_BIT);
    gpu._queueFamilies._transfer = findQueueFamilyIndex(gpu._queueFamiliesProps.data(), gpu._queueFamiliesProps.size(), VK_QUEUE_TRANSFER_BIT);
  }
}

void nakv::VulkanContext::selectPhysicalDevice ()
{
  // Selection of a physical device could be improved giving scores to each device
  // std::vector<uint32_t> scores(_gpus.size()); 

  for (size_t i = 0; i < _gpus.size(); i++)
  {
    GPUInfo& gpu = _gpus[i];

    // scores[i] = 0;

    if (!checkDeviceExtensionSupport(gpu._extensions.data(), gpu._extensions.size())) {
      continue;
    }

    if (gpu._surfaceFormats.size() == 0) {
      continue;
    }

    if (gpu._presentModes.size() == 0) {
      continue;
    }

    // scores[i] = 1; 

    if (gpu._queueFamilies._graphics != UINT32_MAX && gpu._queueFamilies._present != UINT32_MAX)
    {
      _selectedGPUIndex = static_cast<uint32_t>(i);
      return;
      // scores[i] += 1;
    }

    // if (gpu._queueFamilies._transfer != gpu._queueFamilies._graphics)
    // {
      // scores[i] += 1;
    // }

    // if (gpu._queueFamilies._compute != gpu._queueFamilies._graphics)
    // {
      // scores[i] += 1;
    // }
  }

  // uint32_t highestScore = 0;
  // uint32_t gpuIndex = UINT32_MAX;
  // for (size_t i = 0; i < scores.size(); i++)
  // {
    // if (scores[i] > highestScore)
    // {
      // highestScore = scores[i];
      // gpuIndex = i;
    // }
  // }

  // if (gpuIndex == UINT32_MAX)
  // {
    // throw std::runtime_error("Could not find a physical device which fits our desired profile!");
  // }

  // _selectedGPUIndex = gpuIndex;
  // return

  throw std::runtime_error("Could not find a physical device which fits our desired profile!");
}

void nakv::VulkanContext::createCommandBuffers ()
{
  _commandBuffers.resize(VulkanContext::FRAMES_IN_FLIGHT);
  _device.allocateCommandBuffers(_device._commandPools._graphics, VulkanContext::FRAMES_IN_FLIGHT, _commandBuffers.data());
}

void nakv::VulkanContext::createSemaphores ()
{
  _acquireSemaphores.resize(VulkanContext::FRAMES_IN_FLIGHT);
  _renderCompleteSemaphores.resize(VulkanContext::FRAMES_IN_FLIGHT);

  VkSemaphoreCreateInfo semaphoreInfo = {};
  semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

  for (size_t i = 0; i < VulkanContext::FRAMES_IN_FLIGHT; i++)
  {
    NAKV_VALIDATE(vkCreateSemaphore(_device, &semaphoreInfo, nullptr, &_acquireSemaphores[i]), "Could not create acquireSemaphore");
    NAKV_VALIDATE(vkCreateSemaphore(_device, &semaphoreInfo, nullptr, &_renderCompleteSemaphores[i]), "Could not create acquireSemaphore");
  }
}

void nakv::VulkanContext::createFences ()
{
  _commandBufferFences.resize(VulkanContext::FRAMES_IN_FLIGHT);

  VkFenceCreateInfo fenceInfo = {};
  fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

  for (size_t i = 0; i < VulkanContext::FRAMES_IN_FLIGHT; i++)
  {
    NAKV_VALIDATE(vkCreateFence(_device, &fenceInfo, nullptr, &_commandBufferFences[i]), "Could not create commandBufferFence");
  }
}

void nakv::VulkanContext::createRenderPass ()
{

  VkAttachmentDescription colorAttachment = {};
  colorAttachment.format = _swapchain._properties._surfaceFormat.format;
  colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
  colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
  colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
  colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
  colorAttachment.flags = 0;

  VkAttachmentDescription depthAttachment = {};
  depthAttachment.format = _swapchain._depth._format;
  depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
  depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
  depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
  depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
  depthAttachment.flags = 0;

  std::vector<VkAttachmentDescription> attachments = {
    colorAttachment,
    depthAttachment
  };

  VkAttachmentReference colorRef = {};
  colorRef.attachment = 0;
  colorRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

  VkAttachmentReference depthRef = {};
  depthRef.attachment = 1;
  depthRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

  VkSubpassDescription subpass = {};
  subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
  subpass.colorAttachmentCount = 1;
  subpass.pColorAttachments = &colorRef;
  subpass.pDepthStencilAttachment = &depthRef;
  subpass.flags = 0;

  VkRenderPassCreateInfo renderPassInfo = {};
  renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
  renderPassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
  renderPassInfo.pAttachments = attachments.data();
  renderPassInfo.subpassCount = 1;
  renderPassInfo.pSubpasses = &subpass;
  renderPassInfo.dependencyCount = 0;
  renderPassInfo.flags = 0;

  NAKV_VALIDATE(
    vkCreateRenderPass(_device, &renderPassInfo, nullptr, &_renderPass),
    "vkCreateRenderPass could not create render pass"
  );
}

void nakv::VulkanContext::destroyDebugUtilsMessengerEXT (VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator)
{
  auto func = (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance,  "vkDestroyDebugUtilsMessengerEXT");
  if (func != nullptr) {
    func(instance, debugMessenger, pAllocator);
  }
}

bool nakv::VulkanContext::checkValidationLayerSupport (const std::vector<const char*> validationLayers)
{
  uint32_t layersCount;
  vkEnumerateInstanceLayerProperties(&layersCount, nullptr);

  std::vector<VkLayerProperties> availableLayers(layersCount);
  vkEnumerateInstanceLayerProperties(&layersCount, availableLayers.data());

  for (const char* layerName : validationLayers) {
    bool layerFound = false;

    for (const auto& layerProperties : availableLayers) {
      if (strcmp(layerName, layerProperties.layerName) == 0) {
        layerFound = true;
        break;
      }
    }

    if (!layerFound) {
      return false;
    }

  }

  return true;
}

std::vector<const char*> nakv::VulkanContext::getRequiredExtensions (const bool validationLayersEnabled)
{
  uint32_t glfwExtensionCount = 0;
  const char** glfwExtensions;
  glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

  std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

  if (validationLayersEnabled) {
    extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
  }

  return extensions;
}

void nakv::VulkanContext::populateDebugMessengerCreateInfo (VkDebugUtilsMessengerCreateInfoEXT& createInfo)
{
  createInfo = {};
  createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
  createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
  createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
  createInfo.pfnUserCallback = nakv::VulkanContext::debugCallback;
  createInfo.pUserData = nullptr;
}

VkResult nakv::VulkanContext::createDebugUtilsMessengerEXT (
  const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo,
  const VkAllocationCallbacks* pAllocator,
  VkDebugUtilsMessengerEXT* pDebugMessenger
)
{  
  auto func = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr(
    _instance,
    "vkCreateDebugUtilsMessengerEXT"
  );

  if (func != nullptr) {
    return func(_instance, pCreateInfo, pAllocator, pDebugMessenger);
  }
  else {
    return VK_ERROR_EXTENSION_NOT_PRESENT;
  }
}


// QueueFamilyIndices nakv::VulkanContext::findQueueFamilies (VkPhysicalDevice device) {
//   QueueFamilyIndices indices;

//   uint32_t queueFamilyCount = 0;
//   vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);

//   std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
//   vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

//   int i = 0;
//   for (const auto& queueFamily : queueFamilies) {
//     if (queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
//       indices._graphicsFamily = i;
//     }

//     VkBool32 presentSupport = false;
//     vkGetPhysicalDeviceSurfaceSupportKHR(device, i, _window->_surface, &presentSupport);

//     if (queueFamily.queueCount > 0 && presentSupport) {
//       indices._presentFamily = i;
//     }

//     if (queueFamily.queueCount > 0 && (queueFamily.queueFlags & VK_QUEUE_TRANSFER_BIT) && !(queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT)) {
//       indices._transferFamily = i;
//     }

//     if (indices.isComplete()) {
//         break;
//     }

//     i++;
//   }

//   // If device has no dedicated transfer queue, assign graphicsFamily index
//   if (!indices._transferFamily.has_value()) {
//     indices._transferFamily = indices._graphicsFamily;
//   }
  
//   return indices;
// }

bool nakv::VulkanContext::isGPUSuitable (const GPUInfo& gpuInfo)
{
  // VkPhysicalDeviceProperties deviceProperties;
  // vkGetPhysicalDeviceProperties(m_device, &deviceProperties);

  // VkPhysicalDeviceFeatures deviceFeatures;
  // vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

  // bool extensionsSupported = checkDeviceExtensionSupport(device);

  // QueueFamilyIndices indices = findQueueFamilies(device);  

  // bool swapChainAdequate = false;
  // if (extensionsSupported) {
  //   SwapChainSupportDetails swapChainSupport = querySwapChainSupport(device);
  //   swapChainAdequate = !swapChainSupport.formats.empty() && !swapChainSupport._presentModes.empty();
  // }

  return true;

  // return indices.isComplete() && extensionsSupported && swapChainAdequate && deviceFeatures.samplerAnisotropy;
}

bool nakv::VulkanContext::checkDeviceExtensionSupport (const VkExtensionProperties* availableExtensions, const uint32_t extensionCount)
{
  std::set<std::string> requiredExtensions(
    DEVICE_EXTENSIONS.begin(), DEVICE_EXTENSIONS.end()
  );

  for (size_t i = 0; i < extensionCount; i++)
  {
    requiredExtensions.erase(availableExtensions[i].extensionName);
  }

  return requiredExtensions.empty();
}