
#include <NakV/VulkanContext.h>
#include <NakV/VulkanWindow.h>

#include <iostream>
#include <memory>

nakv::VulkanWindow* window;
nakv::VulkanContext* vulkan;

void run () {
  window = new nakv::VulkanWindow(1920, 1080, "NakV Test");
  window->init();

  vulkan = new nakv::VulkanContext(window);
  vulkan->_applicationName = "NakV Test";
  vulkan->_applicationVersion = VK_MAKE_VERSION(1, 0, 0);
  #ifndef NODEBUG
    vulkan->_validationLayersEnabled = true;
  #endif
  vulkan->_validationLayers = {
    "VK_LAYER_LUNARG_standard_validation",
    "VK_LAYER_KHRONOS_validation"
  };
  vulkan->init();

  while(!window->shouldClose()) {
    glfwPollEvents();
  }

  vulkan->cleanup();
  delete vulkan;
  delete window;
  return;
}

int main () {

  run();
  glfwTerminate();

  return 0;
}