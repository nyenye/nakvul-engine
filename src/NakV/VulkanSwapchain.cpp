
#include <NakV/VulkanSwapchain.h>

#include <vulkan/vulkan.h>

#include <NakV/VulkanValidator.h>
#include <NakV/VulkanContext.h>
#include <NakV/VulkanDevice.h>
#include <NakV/VulkanWindow.h>

nakv::VulkanSwapchain::VulkanSwapchain()
{
}

nakv::VulkanSwapchain::VulkanSwapchain (const VulkanContext* context)
{
  _context = context;
  _window = context->_window;
  _device = &(context->_device);
}

nakv::VulkanSwapchain::~VulkanSwapchain ()
{
}

void nakv::VulkanSwapchain::init(const VulkanContext* context)
{
  _context = context;
  _window = context->_window;
  _device = &(context->_device);

  this->init();
}

void nakv::VulkanSwapchain::init()
{
  _properties._surfaceFormat = this->chooseSurfaceFormat();
  _properties._presentMode = this->choosePresentMode();
  _properties._extent = this->chooseSurfaceExtent();

  VkSwapchainCreateInfoKHR swapchainInfo = {};
  swapchainInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
  swapchainInfo.surface = _window->_surface;
  swapchainInfo.imageArrayLayers = 1; //For non-stereoscopic-3D applications, this value is 1.
  swapchainInfo.minImageCount = VulkanContext::FRAMES_IN_FLIGHT;
  
  swapchainInfo.imageFormat = _properties._surfaceFormat.format;
  swapchainInfo.imageColorSpace = _properties._surfaceFormat.colorSpace;
  swapchainInfo.imageExtent = _properties._extent;
  swapchainInfo.presentMode = _properties._presentMode;
  
  // Is Vulkan allowed to discard operations outside of the renderable space?
  swapchainInfo.clipped = VK_TRUE;

  // We just want to leave the image as is.
  swapchainInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
	swapchainInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

  // VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT - This is a color image I'm rendering into.
	// VK_IMAGE_USAGE_TRANSFER_SRC_BIT - I'll be copying this image somewhere. ( screenshot, postprocess )
  swapchainInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;

  std::vector<uint32_t> familyIndices = {
    _device->_queueIndices->_graphics
  };

  if (_device->_queueIndices->hasDedicatedPresentQueue())
  {
    familyIndices.push_back(_device->_queueIndices->_present);
    swapchainInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    swapchainInfo.queueFamilyIndexCount = static_cast<uint32_t>(familyIndices.size());
    swapchainInfo.pQueueFamilyIndices = familyIndices.data();
  }
  else
  {
    swapchainInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    swapchainInfo.queueFamilyIndexCount = static_cast<uint32_t>(familyIndices.size());
    swapchainInfo.pQueueFamilyIndices = familyIndices.data();
  }

  NAKV_VALIDATE(
    vkCreateSwapchainKHR(_device->_logicalDevice, &swapchainInfo, nullptr, &_swapchain),
    "Could not create swapchain"
  );

  this->initResourceImagesAndViews();
  this->initFramebuffers();
}

void nakv::VulkanSwapchain::cleanup()
{
  vkDestroyImageView(_device->_logicalDevice, _depth._imageView, nullptr);
  vkDestroyImage(_device->_logicalDevice, _depth._image, nullptr);
  vkFreeMemory(_device->_logicalDevice, _depth._imageMemory, nullptr);

  for (size_t i = 0; i < _resources._imageViews.size(); i++)
  {
    vkDestroyImageView(_device->_logicalDevice, _resources._imageViews[i], nullptr);
  }

  vkDestroySwapchainKHR(_device->_logicalDevice, _swapchain, nullptr);
}

VkSurfaceFormatKHR nakv::VulkanSwapchain::chooseSurfaceFormat()
{
  VkSurfaceFormatKHR result;
  
  uint32_t formatCount = _device->_gpu->_surfaceFormats.size();
  VkSurfaceFormatKHR* formats = _device->_gpu->_surfaceFormats.data();

	// If Vulkan returned an unknown format, then just force what we want.
  if (formatCount == 1 && formats[0].format == VK_FORMAT_UNDEFINED)
  {
    result.colorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
    result.format = VK_FORMAT_B8G8R8A8_UNORM;
    return result;
  }

	// Favor 32 bit rgba and srgb nonlinear colorspace
  for (size_t i = 0; i < formatCount; i++)
  {
    if (
      formats[i].format == VK_FORMAT_B8G8R8A8_UNORM &&
      formats[i].colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR
    )
    {
      return formats[i];
    }
  }

  // If all else fails, just return what's available
  return formats[0];
}

VkPresentModeKHR nakv::VulkanSwapchain::choosePresentMode()
{
  const VkPresentModeKHR desiredMode = VK_PRESENT_MODE_MAILBOX_KHR;

  uint32_t presentModeCount = _device->_gpu->_presentModes.size();
  VkPresentModeKHR* presentModes = _device->_gpu->_presentModes.data();

  // Favor looking for mailbox mode.
	for (size_t i = 0; i < presentModeCount; ++i) {
		if (presentModes[i] == desiredMode ) {
			return desiredMode;
		}
	}

	// If we couldn't find mailbox, then default to FIFO which is always available.
  return VK_PRESENT_MODE_FIFO_KHR;
}

VkExtent2D nakv::VulkanSwapchain::chooseSurfaceExtent()
{
  VkSurfaceCapabilitiesKHR capabilities = _device->_gpu->_capabilities;

  if (capabilities.currentExtent.width != UINT32_MAX) {
    return capabilities.currentExtent;
  }

  int width, height;
  glfwGetFramebufferSize(_window->_glfw, &width, &height);

  VkExtent2D extent = {
    static_cast<uint32_t>(width),
    static_cast<uint32_t>(height)
  };

  extent.width = std::max(
    capabilities.minImageExtent.width,
    std::min(capabilities.maxImageExtent.width, extent.width)
  );

  extent.height = std::max(
    capabilities.minImageExtent.height,
    std::min(capabilities.maxImageExtent.height, extent.height)
  );

  return extent;
}

void nakv::VulkanSwapchain::initResourceImagesAndViews ()
{
  // Swapchain Images and Views
  uint32_t imageCount;
  vkGetSwapchainImagesKHR(_device->_logicalDevice, _swapchain, &imageCount, nullptr);
  _resources._images.resize(imageCount);  

  NAKV_VALIDATE(
    vkGetSwapchainImagesKHR(_device->_logicalDevice, _swapchain, &imageCount, _resources._images.data()),
    "vkGetSwapchainImagesKHR failed to retrieve swapchain images"
  );
  NAKV_CHECK(imageCount > 0, "vkGetSwapchainImagesKHR returned a zero image count.");

  _resources._imageViews.resize(imageCount);

  for (size_t i = 0; i < imageCount; i++)
  {
    _device->createImageView(
      _resources._images[i],
      _resources._imageViews[i],
      _properties._surfaceFormat.format,
      VK_IMAGE_ASPECT_COLOR_BIT,
      1
    );
  }

  // Depth Image and View
  VkFormat formats[] = {
    VK_FORMAT_D32_SFLOAT_S8_UINT,
    VK_FORMAT_D24_UNORM_S8_UINT
  };

  _depth._format = _device->chooseSupportedFormat(formats, 2, VK_IMAGE_TILING_OPTIMAL, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);

  std::vector<uint32_t> familyIndices = {
    _device->_queueIndices->_graphics
  };

  if (_device->_queueIndices->hasDedicatedTransferQueue())
  {
    familyIndices.push_back(_device->_queueIndices->_transfer);
  }

  _device->createImage(
    _properties._extent.width, _properties._extent.height, 1,
    _depth._format, VK_IMAGE_TILING_OPTIMAL, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
    familyIndices.size(), familyIndices.data(),
    VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
    _depth._image, _depth._imageMemory
  );

  _device->createImageView(_depth._image, _depth._imageView, _depth._format, VK_IMAGE_ASPECT_DEPTH_BIT, 1);

  _device->transitionImageLayout(
    _depth._image, _depth._format,
    VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
    1
  );
}

void nakv::VulkanSwapchain::initFramebuffers ()
{

}