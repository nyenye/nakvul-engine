#include <NakV/VulkanValidator.h>

#include <iostream>

void nakv::NAKV_CHECK (bool check, const char* errorMsg)
{

  if (!check)
  {
    throw std::runtime_error(errorMsg);
  }
  
  return;
}

void nakv::NAKV_VALIDATE (VkResult result, const char* errorMsg)
{

  if (result == VK_EVENT_SET || result == VK_EVENT_RESET)
  {
    return;
  }

  if (result == VK_INCOMPLETE || result == VK_NOT_READY || result == VK_TIMEOUT ) {
    std::cout << "Return code: " <<  result << std::endl;
    return;
  }

  // ERROR CODES
  if (result != VK_SUCCESS)
  {
    std::cout << "Error code: " <<  result << std::endl;
    throw std::runtime_error(errorMsg);
  }

  // VK_SUCCESS
  return;
}