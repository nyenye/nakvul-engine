
#include <NakV/VulkanDevice.h>

#include <vulkan/vulkan.h>

#include <NakV/VulkanContext.h>
#include <NakV/VulkanValidator.h>

nakv::VulkanDevice::VulkanDevice ()
{
}

nakv::VulkanDevice::VulkanDevice (const VulkanContext* context, GPUInfo* gpu)
{
  _context = context;
  _gpu = gpu;
  _queueIndices = &(gpu->_queueFamilies);
}

nakv::VulkanDevice::~VulkanDevice ()
{
}

bool nakv::VulkanDevice::hasStencilComponent (VkFormat format) {
  return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
} 

void nakv::VulkanDevice::init(const VulkanContext* context, GPUInfo* gpu)
{
  _context = context;
  _gpu = gpu;
  _queueIndices = &(gpu->_queueFamilies);

  this->init();
}

void nakv::VulkanDevice::init ()
{
  VkDeviceCreateInfo deviceInfo = {};
  deviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;

  std::vector<VkDeviceQueueCreateInfo> queuesInfo;
  float queuePriority = 0.0f;

  // If has graphics queue - Must always have it
  VkDeviceQueueCreateInfo graphicsQueueInfo;
  if (_queueIndices->hasGraphicsQueue())
  {
    graphicsQueueInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    graphicsQueueInfo.pNext = nullptr;
    graphicsQueueInfo.flags = 0;
    graphicsQueueInfo.pQueuePriorities = &queuePriority;
    graphicsQueueInfo.queueCount = 1;
    graphicsQueueInfo.queueFamilyIndex = _queueIndices->_graphics;
    
    queuesInfo.push_back(graphicsQueueInfo);
  }

  // If has dedicated present queue
  VkDeviceQueueCreateInfo presentQueueInfo;
  if (_queueIndices->hasDedicatedPresentQueue())
  {
    presentQueueInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    presentQueueInfo.pNext = nullptr;
    presentQueueInfo.flags = 0;
    presentQueueInfo.pQueuePriorities = &queuePriority;
    presentQueueInfo.queueCount = 1;
    presentQueueInfo.queueFamilyIndex = _queueIndices->_present;
    
    queuesInfo.push_back(presentQueueInfo);
  }

  // If has dedicated transfer queue
  VkDeviceQueueCreateInfo transferQueueInfo;
  if (_queueIndices->hasDedicatedTransferQueue())
  {
    transferQueueInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    transferQueueInfo.pNext = nullptr;
    transferQueueInfo.flags = 0;
    transferQueueInfo.pQueuePriorities = &queuePriority;
    transferQueueInfo.queueCount = 1;
    transferQueueInfo.queueFamilyIndex = _queueIndices->_transfer;
    
    queuesInfo.push_back(transferQueueInfo);
  }

  deviceInfo.queueCreateInfoCount = static_cast<uint32_t>(queuesInfo.size());
  deviceInfo.pQueueCreateInfos = queuesInfo.data();

  deviceInfo.enabledExtensionCount = static_cast<uint32_t>(_context->DEVICE_EXTENSIONS.size());
  deviceInfo.ppEnabledExtensionNames = _context->DEVICE_EXTENSIONS.data();

  if (_context->_validationLayersEnabled)
  {
    deviceInfo.enabledLayerCount = static_cast<uint32_t>(_context->_validationLayers.size());
    deviceInfo.ppEnabledLayerNames = _context->_validationLayers.data();
  }
  else
  {
    deviceInfo.enabledLayerCount = 0;
    deviceInfo.ppEnabledLayerNames = nullptr;
  }
  
  // For a list of available features: https://www.khronos.org/registry/vulkan/specs/1.1-extensions/man/html/VkPhysicalDeviceFeatures.html
  VkPhysicalDeviceFeatures deviceFeatures = {};
  deviceFeatures.samplerAnisotropy = VK_TRUE;
  
  deviceInfo.pEnabledFeatures = &deviceFeatures;

  NAKV_VALIDATE(
    vkCreateDevice(_gpu->_physicalDevice, &deviceInfo, nullptr, &_logicalDevice),
    "Logical device could not be created!"
  );

  if (_queueIndices->hasGraphicsQueue())
  {
    vkGetDeviceQueue(_logicalDevice, _queueIndices->_graphics, 0, &_queues._graphics);
  }

  if (_queueIndices->hasPresentQueue())
  {
    vkGetDeviceQueue(_logicalDevice, _queueIndices->_present, 0, &_queues._present);
  }

  if (_queueIndices->hasDedicatedTransferQueue())
  {
    vkGetDeviceQueue(_logicalDevice, _queueIndices->_transfer, 0, &_queues._transfer);
  }

  // Initialize Command Pools
  this->initCommandPools();
}

void nakv::VulkanDevice::allocateCommandBuffers (VkCommandPool commandPool, const uint32_t bufferCount, VkCommandBuffer* pCommandBuffer)
{
  VkCommandBufferAllocateInfo commandBufferAllocateInfo = {};
  commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  commandBufferAllocateInfo.commandPool = commandPool;
  commandBufferAllocateInfo.commandBufferCount = bufferCount;

  NAKV_VALIDATE(
    vkAllocateCommandBuffers(_logicalDevice, &commandBufferAllocateInfo, pCommandBuffer),
    "Could not allocate command buffers"
  );
}

void nakv::VulkanDevice::createImage (
  uint32_t width, uint32_t height, uint32_t mipLevels,
  VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, 
  uint32_t queueFamilyIndexCount, const uint32_t* pQueueFamilyIndices,
  VkMemoryPropertyFlags memoryProps,
  VkImage& image, VkDeviceMemory& imageMemory
) const {

  VkImageCreateInfo imageInfo = {};
  imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
  imageInfo.imageType = VK_IMAGE_TYPE_2D;
  imageInfo.extent.width = static_cast<uint32_t>(width);
  imageInfo.extent.height = static_cast<uint32_t>(height);
  imageInfo.extent.depth = 1;
  imageInfo.mipLevels = mipLevels;
  imageInfo.arrayLayers = 1;
  imageInfo.format = format;
  imageInfo.tiling = tiling;
  imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  imageInfo.usage = usage;
  imageInfo.sharingMode = VK_SHARING_MODE_CONCURRENT;
  imageInfo.queueFamilyIndexCount = queueFamilyIndexCount;
  imageInfo.pQueueFamilyIndices = pQueueFamilyIndices;
  imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
  imageInfo.flags = 0;

  NAKV_VALIDATE(
    vkCreateImage(_logicalDevice, &imageInfo, nullptr, &image),
    "vkCreateImage failed to create image"
  );

  VkMemoryRequirements memRequirements;
  vkGetImageMemoryRequirements(_logicalDevice, image, &memRequirements);

  VkMemoryAllocateInfo allocInfo = {};
  allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
  allocInfo.allocationSize = memRequirements.size;
  allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, memoryProps);

  if (vkAllocateMemory(_logicalDevice, &allocInfo, nullptr, &imageMemory) != VK_SUCCESS) {
    throw std::runtime_error("failed to allocate image memory!");
  }

  vkBindImageMemory(_logicalDevice, image, imageMemory, 0);
}

VkImageView nakv::VulkanDevice::createImageView (VkImage image, VkFormat format, VkImageAspectFlags aspectFlags, uint32_t mipLevels) const
{
  VkImageViewCreateInfo imageViewInfo = {};
  imageViewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
  imageViewInfo.image = image;
  imageViewInfo.format = format;
  imageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;

  imageViewInfo.components.r = VK_COMPONENT_SWIZZLE_R;
  imageViewInfo.components.g = VK_COMPONENT_SWIZZLE_G;
  imageViewInfo.components.b = VK_COMPONENT_SWIZZLE_B;
  imageViewInfo.components.a = VK_COMPONENT_SWIZZLE_A;
  
  imageViewInfo.subresourceRange.aspectMask = aspectFlags;
  imageViewInfo.subresourceRange.baseMipLevel = 0;
  imageViewInfo.subresourceRange.levelCount = mipLevels;
  imageViewInfo.subresourceRange.baseArrayLayer = 0;
  imageViewInfo.subresourceRange.layerCount = 1;

  imageViewInfo.flags = 0;

  VkImageView imageView;

  NAKV_VALIDATE(
    vkCreateImageView(_logicalDevice, &imageViewInfo, nullptr, &imageView),
    "vkCreateImageView failed to create image view"
  );

  return imageView;
}

void nakv::VulkanDevice::createImageView (VkImage image, VkImageView& imageView, VkFormat format, VkImageAspectFlags aspectFlags, uint32_t mipLevels) const
{
  VkImageViewCreateInfo imageViewInfo = {};
  imageViewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
  imageViewInfo.image = image;
  imageViewInfo.format = format;
  imageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;

  imageViewInfo.components.r = VK_COMPONENT_SWIZZLE_R;
  imageViewInfo.components.g = VK_COMPONENT_SWIZZLE_G;
  imageViewInfo.components.b = VK_COMPONENT_SWIZZLE_B;
  imageViewInfo.components.a = VK_COMPONENT_SWIZZLE_A;
  
  imageViewInfo.subresourceRange.aspectMask = aspectFlags;
  imageViewInfo.subresourceRange.baseMipLevel = 0;
  imageViewInfo.subresourceRange.levelCount = mipLevels;
  imageViewInfo.subresourceRange.baseArrayLayer = 0;
  imageViewInfo.subresourceRange.layerCount = 1;

  imageViewInfo.flags = 0;

  NAKV_VALIDATE(
    vkCreateImageView(_logicalDevice, &imageViewInfo, nullptr, &imageView),
    "vkCreateImageView failed to create image view"
  );
}

VkFormat nakv::VulkanDevice::chooseSupportedFormat (VkFormat* formats, uint32_t formatCount, VkImageTiling tiling, VkFormatFeatureFlags features) const
{
  for (size_t i = 0; i < formatCount; i++)
  {
    VkFormatProperties props;
    vkGetPhysicalDeviceFormatProperties(_gpu->_physicalDevice, formats[i], &props);

    if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features)
    {
      return formats[i];
    }
    else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features)
    {
      return formats[i];
    }
  }

  std::runtime_error("Failed to find a supported format.");

  return VK_FORMAT_UNDEFINED;
}

void nakv::VulkanDevice::transitionImageLayout (
  VkImage image, VkFormat format,
  VkImageLayout oldLayout, VkImageLayout newLayout,
  uint32_t mipLevels
) const {
  VkCommandBuffer commandBuffer = beginSingleTimeCommands(_commandPools._graphics);

  VkImageMemoryBarrier barrier = {};
  barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
  barrier.oldLayout = oldLayout;
  barrier.newLayout = newLayout;

  barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

  barrier.image = image;
  if (newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
    barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;

    if (VulkanDevice::hasStencilComponent(format)) {
      barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
    }
  } 
  else {
    barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
  }
  barrier.subresourceRange.baseMipLevel = 0;
  barrier.subresourceRange.levelCount = mipLevels;
  barrier.subresourceRange.baseArrayLayer = 0;
  barrier.subresourceRange.layerCount = 1;

  VkPipelineStageFlags sourceStage;
  VkPipelineStageFlags destinationStage;

  if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
    barrier.srcAccessMask = 0;
    barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

    sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
    destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
  } else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
    barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

    sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
  } else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
    barrier.srcAccessMask = 0;
    barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

    sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
    destinationStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
  }  else {
    throw std::invalid_argument("unsupported layout transition!");
  }

  vkCmdPipelineBarrier(
    commandBuffer,
    sourceStage, destinationStage,
    0,
    0, nullptr,
    0, nullptr,
    1, &barrier
  );

  endSingleTimeCommands(_commandPools._graphics, _queues._graphics, commandBuffer);
}

void nakv::VulkanDevice::cleanup ()
{
  if (_commandPools._graphics != VK_NULL_HANDLE)
  {
    vkDestroyCommandPool(_logicalDevice, _commandPools._graphics, nullptr);
  }

  if (_commandPools._transfer != VK_NULL_HANDLE)
  {
    vkDestroyCommandPool(_logicalDevice, _commandPools._transfer, nullptr);
  }

  if (_logicalDevice)
  {
    vkDestroyDevice(_logicalDevice, nullptr);
  }
}

// private:

void nakv::VulkanDevice::initCommandPools ()
{
  {
    VkCommandPoolCreateInfo graphicsCommandPoolInfo = {};
    graphicsCommandPoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    graphicsCommandPoolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    graphicsCommandPoolInfo.queueFamilyIndex = _queueIndices->_graphics;

    NAKV_VALIDATE(
      vkCreateCommandPool(_logicalDevice, &graphicsCommandPoolInfo, nullptr, &_commandPools._graphics),
      "Could not create graphics Command Pool"
    );
  }

  if (_queueIndices->_transfer != UINT32_MAX && _queueIndices->_transfer != _queueIndices->_graphics)
  {
    VkCommandPoolCreateInfo transferCommandPoolInfo = {};
    transferCommandPoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    transferCommandPoolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    transferCommandPoolInfo.queueFamilyIndex = _queueIndices->_transfer;

    NAKV_VALIDATE(
      vkCreateCommandPool(_logicalDevice, &transferCommandPoolInfo, nullptr, &_commandPools._transfer),
      "Could not create transfer Command Pool"
    );
  }
}

uint32_t nakv::VulkanDevice::findMemoryType (uint32_t typeFilter, VkMemoryPropertyFlags properties) const
{
  VkPhysicalDeviceMemoryProperties memProperties;
  vkGetPhysicalDeviceMemoryProperties(_gpu->_physicalDevice, &memProperties);

  for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
    if (typeFilter & (1 << i) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
      return i;
    }
  }

  throw std::runtime_error("failed to find suitable memory type");  
}

VkCommandBuffer nakv::VulkanDevice::beginSingleTimeCommands(VkCommandPool commandPool) const
{
  VkCommandBufferAllocateInfo allocInfo = {};
  allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  allocInfo.commandPool = commandPool;
  allocInfo.commandBufferCount = 1;

  VkCommandBuffer commandBuffer;
  vkAllocateCommandBuffers(_logicalDevice, &allocInfo, &commandBuffer);

  VkCommandBufferBeginInfo beginInfo = {};
  beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

  vkBeginCommandBuffer(commandBuffer, &beginInfo);

  return commandBuffer;
}

void nakv::VulkanDevice::endSingleTimeCommands(VkCommandPool commandPool, VkQueue queue, VkCommandBuffer commandBuffer) const
{
  vkEndCommandBuffer(commandBuffer);

  VkSubmitInfo submitInfo = {};
  submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &commandBuffer;

  vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);
  vkQueueWaitIdle(queue);

  vkFreeCommandBuffers(_logicalDevice, commandPool, 1, &commandBuffer);
}